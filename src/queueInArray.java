public class queueInArray {
    int MAX_SIZE=30;
    int arr[];
    int front,rear;
    queueInArray(){
        arr=new int[MAX_SIZE];
        front=-1;
        rear=-1;
    }
    public void enqueue(int val){
        if(rear==MAX_SIZE)
            throw new IndexOutOfBoundsException("queue is full");
        if(front==-1)
            ++front;
        arr[++rear]=val;
    }
    public int dequeue(){
        if(rear==-1 ||front>rear)
            throw new IndexOutOfBoundsException("queue is empty");

        return arr[front++];
    }
}
